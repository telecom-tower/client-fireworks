#!/bin/sh

mkdir -p ${GOPATH}/src/gitlab.com/${CI_PROJECT_NAMESPACE}
ln -s "${CI_PROJECT_DIR}" "${GOPATH}/src/gitlab.com/${CI_PROJECT_PATH}"
