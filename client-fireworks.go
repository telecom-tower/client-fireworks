package main

import (
	"container/list"
	"flag"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/geomyidae/ws2811client"
	"gitlab.com/telecom-tower/font"
	"gitlab.com/telecom-tower/pixtrix"
)

const (
	columns           = 128
	rows              = 8
	defaultBrightness = 128
	nOfFrames         = 3
	bombsThreshold    = 0.5
)

type bomb struct {
	row    int
	column int
	frame  int
	color  uint32
}

func makeFireworks(ws *ws2811client.Ws2811Client, rows int, columns int) {

	frames := make([]*pixtrix.Pixtrix, nOfFrames)

	for i := range frames {
		frames[i] = &pixtrix.Pixtrix{
			Rows:   rows,
			Bitmap: make([]uint32, rows*columns),
		}
	}

	frameNr := make(chan int, nOfFrames)
	bombs := list.New()

	// Frame producer
	go func() {
		i := 0
		for {
			// clear frame
			for x := 0; x < columns; x++ {
				for y := 0; y < rows; y++ {
					frames[i].SetPixel(x, y, 0x000000)
				}
			}

			w := pixtrix.NewWriter(frames[i])
			msg := "Bonne année 2018"
			w.Spacer(64-len(msg)*3, 0x0000)
			w.WriteText(msg, font.Font6x8, 0xff6600, 0x000000)

			// Create a bomb if required
			if rand.Float32() > bombsThreshold {
				b := new(bomb)
				b.color = colors[rand.Intn(len(colors))]
				b.frame = 0
				b.column = rand.Intn(columns)
				b.row = rand.Intn(rows)
				bombs.PushBack(b)
			}

			// Draw bombs
			e := bombs.Front()
			for e != nil {
				next := e.Next()
				b, _ := e.Value.(*bomb)

				if b.frame < len(scenes) {
					scene := scenes[b.frame]
					h := len(scene)
					w := len(scene[0])
					for x := 0; x < w; x++ {
						for y := 0; y < h; y++ {
							px := b.column - (w-1)/2 + x
							py := b.row - (h-1)/2 + y
							if px < 0 || px >= columns {
								continue
							}
							if py < 0 || py >= rows {
								continue
							}
							if scene[y][x] == '*' {
								frames[i].SetPixel(px, py, b.color)
							}

						}
					}

					// draw bomb
					b.frame++
				}
				if b.frame >= len(scenes) {
					bombs.Remove(e)
				}
				e = next
			}

			frameNr <- i
			i = (i + 1) % 3
		}
	}()

	// Frame consumer
	go func() {
		for {
			i := <-frameNr
			err := ws.Update(frames[i].InterleavedStripes()[0])
			if err != nil {
				os.Exit(0)
			}
			// time.Sleep(50 * time.Millisecond)
		}
	}()

}

func main() {
	var url = flag.String("url", "ws://127.0.0.1:8484/", "URL ot the tower server")
	var brightness = flag.Int("brightness", defaultBrightness, "Brightness between 0 and 255.")
	var debug = flag.Bool("debug", false, "be verbose")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infoln("Starting snowing tower")
	ws, err := ws2811client.LocalClient(*url)
	if err != nil {
		log.Fatalf("Error connecting to tower: %v", err)
	}
	ws.Brightness = *brightness
	ws.Open(1000 * time.Millisecond)

	makeFireworks(ws, rows, columns)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs

	log.Infoln("Main loop terminated!")
	ws.Close()
}
